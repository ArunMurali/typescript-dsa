import { readInput } from "./util/helper";

const cartesianProduct = (n: number[], b: number[]): number[][] => {
  let productArray: number[][] = [];
  for (let i = 0; i < n.length; i++) {
    for (let j = 0; j < b.length; j++) {
      productArray.push([n[i], b[j]]);
    }
  }

  return productArray;
};

async function main(): Promise<void> {
  let a: number[] = JSON.parse(await readInput("enter the A"));
  let b: number[] = JSON.parse(await readInput("enter the B"));
  const res: number[][] = cartesianProduct(a, b);
  console.log("A x B", JSON.stringify( res));
  //   console.log(resDES,'descending sort ')
}
main().catch((reason) => console.error(reason));
