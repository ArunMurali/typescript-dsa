import { readInput } from "./util/helper";
const climbingStaircase = (n: number) => {
  let previousOne = 1;
  let previousTwo = 2;
  if (n == 1) return previousOne;
  if (n == 2) return previousTwo;
  for (let i = 3; i <= n; i++) {
   let temp = previousTwo
    previousTwo = previousOne + previousTwo;
    previousOne = temp;
  }
  return previousTwo;
};

async function main(): Promise<void> {
  const n = await readInput("Number of staircase");
  let input: number = JSON.parse(n);
  const res = climbingStaircase(input);
  console.log(res,)
}
main().catch((reason) => console.error(reason));
