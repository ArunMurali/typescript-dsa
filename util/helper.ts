import * as readline from "readline";

export const readInput = async (question: string): Promise<string> => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise((resolve, reject) => {
    rl.question(question + "\n", (value) => {
      if (value) {
        rl.close();
        resolve(value);
      } else {
        rl.close();
        reject("no input value");
      }
    });
  });
};


