import { readInput } from "./util/helper";
const climbingStaircase = (n: number): number => {
let arr = [1,2];
  if (n == 2) return 2;
  if (n == 1) return 1;

  return climbingStaircase(n - 1) + climbingStaircase(n-2);
};

async function main(): Promise<void> {
  const n = await readInput("Number of staircase");
  let input: number = JSON.parse(n);
  const res = climbingStaircase(input);
  console.log(res);
}
main().catch((reason) => console.error(reason));
