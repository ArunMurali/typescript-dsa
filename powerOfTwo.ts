import {readInput} from "./util/helper";
const powerOfTwo = (n: number): number => {
  if (n == 0) return 1;
  return 2 * powerOfTwo(n - 1);
};
async function main(): Promise<void> {
  const n = await readInput("enter the fibonacci number");
  const res: number = powerOfTwo(parseInt(n));
  console.log(res);
}
main().catch((reason) => console.error(reason));
