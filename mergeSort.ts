import { readInput } from "./util/helper";
type Comparable = number | string;
const mergeSort = (n: Comparable[]): Comparable[] => {
  let length = n.length;
  if (length < 2) return n;
  let mid = Math.floor(length / 2);
  let left: Comparable[] = n.slice(0, mid);
  let right: Comparable[] = n.slice(mid);
  return merge(mergeSort(left), mergeSort(right));
};
const merge = (left: Comparable[], right: Comparable[]) => {
  let sortedArray: any[] = [];
  while (left.length && right.length) {
    if (left[0] < right[0]) {
      sortedArray.push(left.shift());
    } else {
      sortedArray.push(right.shift());
    }
  }

  return [...sortedArray,...left,...right];
};

async function main(): Promise<void> {
  const n = await readInput("enter the array");
  let input: Comparable[] = JSON.parse(n);
  const res: Comparable[] = mergeSort([...input]);
//   const resDES: Comparable[] = insertionDESSort([...input]);
  console.log(res, "accenting sort");
//   console.log(resDES, "decending sort ");
}
main().catch((reason) => console.error(reason));
