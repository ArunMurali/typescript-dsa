# TypeScript Programs

DataStructure using typescript

## Table of Content

- [Power of two](./powerOfTwo.ts)
- [Bubble Sort Ascending and Descending](./bubbleSort.ts)
- [Insertion sort](./insertionSort.ts)
- [Quick Sort](./quickSort.ts)
- [Merge Sort](./mergeSort.ts)
- [Cartesian Product](./cartesianProduct.ts)
- [Climbing the Staircase](#climb)
- [Tower of Hanoi](#hanoi)
- [Section 3](#section3)

## Description

### Climbing the Staircase<a name="climb"></a>
- [using for loop](./climbingStaircase.ts)
- [using recursion](./climbingStaircase_recursion.ts)
- Given a staircase **n** steps, count the number of distinct ways to climb the steps.
  you can climb one step or two step at time.

    * n =1 , climbingStaircase(1) = 1 | (1)
    * n =2 , climbingStaircase(1) = 2 | (1,1),(1,2)
    * n =3 , climbingStaircase(3) = 3 | (1,1,1),(1,2),(2,1)
    * n =4 , climbingStaircase(4) = 5 | (1,1,1,1),(1,1,2),(2,1,1),(1,2,1),(2,2)
    * general for n, climbingStaircase(n) = climbingStaircase(n - 1) + climbingStaircase(n -2)
### [Tower of Hanoi](./towerOfHanoi.ts)<a name="hanoi"></a>

- The objective of the puzzle is to move the entire
  stack to the last rod, obeying the following rules:
  * Only one disk may be moved at a time.
  * Each move consists of taking the upper disk from one
    of the stacks and placing it on top of another stack or
    on an empty rod. And lastly,
  * No disk may be placed on top of a disk that is smaller.

## Compile TypeScript to JavaScript

```cmd
npm ci
npm run compile
```

## To Run test file

```cmd 
npm ci 
npm run test
```

<!-- # Section 1
some text

# Section 2
some text

# Section 3
some text -->
