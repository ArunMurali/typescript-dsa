import { readInput } from "./util/helper";
const towerOfHanoi = (
  n: number,
  fromRod: string,
  usingRod: string,
  toRod: string
): void => {
  if (n == 1) {
    console.log(`move 1 from ${fromRod} to ${toRod}`);
    return;
  }
  towerOfHanoi(n - 1, fromRod, toRod, usingRod);
  console.log(`move ${n} from ${fromRod} to ${toRod}`);
  towerOfHanoi(n - 1, usingRod, toRod, fromRod);
};

async function main(): Promise<void> {
  const n = await readInput("Number of staircase");
  let input: number = JSON.parse(n);
  const res = towerOfHanoi(input, "A", "B", "C");
  console.log(res);
}
main().catch((reason) => console.error(reason));
