import {readInput} from "./util/helper";
type Comparable = number | string;
const insertionASCSort = (n: Comparable[]) => {
  let length = n.length;
  for (let i = 1; i < length; i++) {
    let numberOfInterest: Comparable = n[i];
    for (let j = i-1; j >= 0; j--) {
      if (n[j] > numberOfInterest) {
        n[j+1] = n[j];
        n[j] = numberOfInterest;
      }
    }
  }

  return n
};
const insertionDESSort = (n: Comparable[]) => {
    let length = n.length;
    for (let i = 1; i < length; i++) {
      let numberOfInterest: Comparable = n[i];
      for (let j = i-1; j >= 0; j--) {
        if (n[j] < numberOfInterest) {
          n[j+1] = n[j];
          n[j] = numberOfInterest;
        }
      }
    }
  
    return n
  };
async function main(): Promise<void> {
  const n = await readInput("enter the array");
  let input: Comparable[] = JSON.parse(n);
  const res: Comparable[] = insertionASCSort([...input]);
  const resDES: Comparable[] = insertionDESSort([...input]);
  console.log(res, 'accenting sort');
  console.log(resDES,'decending sort ')
}
main().catch((reason) => console.error(reason));
