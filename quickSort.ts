import { readInput } from "./util/helper";
type Comparable = number | string;
const quickSortASC = (n: Comparable[]): any => {
  let length = n.length;
  if (length < 2) return n;

  let right: Comparable[] = [];
  let left: Comparable[] = [];
  let pivot = n[length - 1];
  for (let i = 0; i < length - 1; i++) {
    if (n[i] > pivot) {
      right.push(n[i]);
    } else {
      left.push(n[i]);
    }
  }
  return [...quickSortASC(left), pivot, ...quickSortASC(right)];
};
const quickSortDesc = (n: Comparable[]): any => {
  let length = n.length;
  if (length < 2) return n;

  let right: Comparable[] = [];
  let left: Comparable[] = [];
  let pivot = n[length - 1];
  for (let i = 0; i < length - 1; i++) {
    if (n[i] < pivot) {
      right.push(n[i]);
    } else {
      left.push(n[i]);
    }
  }
  return [...quickSortDesc(left), pivot, ...quickSortDesc(right)];
};

async function main(): Promise<void> {
  const n = await readInput("enter the array");
  let input: Comparable[] = JSON.parse(n);
  const resAsc: Comparable[] = quickSortASC([...input]);
  const resDes: Comparable[] = quickSortDesc([...input]);
  console.log(resAsc, "accenting sort");
  console.log(resDes, "Descending sort");
}
main().catch((reason) => console.error(reason));
