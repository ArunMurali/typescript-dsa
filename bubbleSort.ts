import {readInput} from "./util/helper";

// Define a type for the elements in the array
type Comparable = number | string;

export const bubbleSortAsc = (arr: Comparable[]): Comparable[] => {
  let length: number = arr.length;
  if (arr && length) {
    for (let i: number = 0; i < length; i++) {
      for (let j = 0; j < length; j++) {
        if (arr[i] > arr[i + j]) {
          let temp: Comparable = arr[i + j];
          arr[i + j] = arr[i];
          arr[i] = temp;
        }
      }
    }
  }
  return arr;
};

export const bubbleSortDesc = (arr: Comparable[]): Comparable[] => {
  let length: number = arr.length;
  if (arr && length) {
    for (let i: number = 0; i < length; i++) {
      for (let j = 0; j < length; j++) {
        if (arr[i] < arr[i + j]) {
          let temp: Comparable = arr[i + j];
          arr[i + j] = arr[i];
          arr[i] = temp;
        }
      }
    }
  }
  return arr;
};

async function main(): Promise<void> {
  const input = await  readInput("Enter the array:");
  const tempArr: Comparable[] = JSON.parse(input);
  const ascSorted = bubbleSortAsc([...tempArr]); // Make a copy to preserve the original array
  const descSorted = bubbleSortDesc([...tempArr]); // Make a copy to preserve the original array

  console.log(ascSorted, "asc sort");
  console.log(descSorted, "desc sort");
}

main().catch((error) => {
  console.error("An error occurred:", error);
});
